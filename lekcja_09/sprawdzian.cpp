#include <iostream>
#include <iomanip>

using namespace std;

int main(){
    int n, m;
    cin >> n >> m;
    int spr[n][m];
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < m; ++j)
            cin >> spr[i][j];

    for (int i = 0; i < n; ++i)
    {
        int suma = 0;
        for (int j = 0; j < m; ++j)
            suma += spr[i][j];//suma punktow ze sprawdzaianow dla ucznia
        cout << suma << endl;
    }

    cout << endl;
    cout << setprecision(2) << fixed;
    for (int i = 0; i < n; ++i)
    {
        int suma = 0;
        for (int j = 0; j < m; ++j)
            suma += spr[i][j];
        cout << double(suma) / n << endl;
    }
}
