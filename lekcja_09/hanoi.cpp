#include <iostream>
using namespace std;

void pozycja_hanoi(int n, unsigned long long r) {
    int wieza[n + 1]; // konfiguracja pier�cieni po r ruchach

    unsigned long long potega_2 = 1;
    for (int i = 1; i <= n; ++i) {
        if (r < potega_2) // potega_2 == 2^(i-1)
            wieza[i] = 0;
        else {
            unsigned long long k = 1 + (r - potega_2) / (2 * potega_2);
            if (i % 2 == 1)
                wieza[i] = k % 3;
            else
                wieza[i] = (3 - k % 3) % 3;
        }
        potega_2 *= 2;
    }

    // na koniec zadbajmy o �adne wypisywanie
    for (int w = 0; w < 3; ++w) {
        cout << w << ": ";
        for (int i = n; i >= 1; --i)
            if (wieza[i] == w)
                cout << i << " ";
        cout << endl;
    }
}

int main() {
    int n; // liczba pier�cieni, n <= 63
    cin >> n;
    unsigned long long r; // liczba ruch�w
    cin >> r;
    pozycja_hanoi(n, r);
}
