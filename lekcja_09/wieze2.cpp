#include <iostream>
#include <algorithm>
using namespace std;

int n;
string szach[1000];

//https://main2.edu.pl/main2/courses/show/6/24/
bool czy_szach() {
    for (int i = 0; i < n; ++i) {
        int ile = 0;
        for (int j = 0; j < n; ++j)
            if (szach[i][j] == 'W')
                ++ile;
        if (ile > 1)
            return true;
    }
    return false;
}

int main() {
    cin >> n;
    for (int i = 0; i < n; ++i)
        cin >> szach[i];
    bool atak = czy_szach();

    for (int i = 0; i < n; ++i)
        for (int j = 0; j < i; ++j)
            swap(szach[i][j], szach[j][i]);

    atak = atak || czy_szach();

    if (atak)
        cout << "TAK" << endl;
    else
        cout << "NIE" << endl;
}
