#include <iostream>
#include <algorithm>
using namespace std;

//zadanie: https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/dwa/
int main(){
    int n, m;//n - liczba marketow, m - liczba produktow do zakupienia
    cin >> n >> m;

    int cena[n][m], minglobal[n];

    for (int i = 0; i < n; ++i)
        for (int j = 0; j < m; ++j)
            cena[i][j] = 0;

    for (int i = 0; i < n; ++i)
        minglobal[i] = 0;

    for (int i = 0; i < n; ++i)
        for (int j = 0; j < m; ++j)
            cin >> cena[i][j];

    int q = 0;
    int minlocal;
    for (int i = 0; i < n; ++i) //sklep
    {
        for (int j = i + 1; j < n ; ++j) //sklep z ktorym porownujemy sklep i-ty
        {
            minlocal = 0;
            for (int k = 0; k < m; k++) //produkty
            {
                cout << "k = " << k << endl;
                minlocal += min(cena[i][k], cena[j][k]);// porownanie cen produktow i-tego i j-tego sklepu ze soba
                cout << "cena[" << i << "][" << k << "] = " << cena[i][k];
                cout << "  " << "cena[" << j << "][" << k << "] = " << cena[j][k];
                cout << "  " << "minlocal = " << minlocal;
                cout << endl;
            }
            minglobal[q] = minlocal;
            q++;
        }
    }


    int wynik = 0;
    wynik = minglobal[0];
    cout << "minglobal[" << 0 << "] = " << minglobal[0] << endl;
    for (int i = 1; i < n; ++i)
    {
        cout << "minglobal[" << i << "] = " << minglobal[i] << endl;
        wynik = min(wynik, minglobal[i]);
    }


    cout << wynik << endl;
}
