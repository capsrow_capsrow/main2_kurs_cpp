#include <iostream>
#include <iomanip>
using namespace std;

//zadanie: https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/kol/
int main(){
    double pi = 3.141592;

    int r;
    cin >> r;
    cout << setprecision(3) << fixed;
    cout << pi * r * r << endl;

    cout << setprecision(3) << fixed;
    cout << 2 * pi * r << endl;
}
