#include <iostream>
using namespace std;

//zadanie: https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/ost/
// wykladnik:   1 2 3 4  5   6    7   8
// 2^n:         2 4 8 16 32 64  128 256
int main(){
    int n;
    cin >> n;

    int wynik, ostatnia;
    wynik = 1;
    if (n == 0)
        wynik = 1;
    else
    {
        wynik = n % 4;
        if (wynik == 1)
            ostatnia = 2;
        else if (wynik == 2)
            ostatnia = 4;
        else if (wynik == 3)
            ostatnia = 8;
        else if (wynik == 0)
            ostatnia = 6;
    }

    cout << ostatnia << endl;
}
