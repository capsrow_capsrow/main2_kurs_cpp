#include <algorithm>
#include <cmath>
#include <iostream>

using namespace std;

//zadanie: https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/tas/
int main(){
    int n = 0; //n - dlugosc ciagu
    cin >> n;


    int liczba[n]; // kolejne wyrazu ciagu
    for (int i = 0; i < n; ++i)
        cin >> liczba[i];

    //trzeba wykona� tyle porownan, ile jest kombinacji bez powtorzen w ciagu
    //bedzie to liczba kombinacji 2-elemntowych zbioru n elementowego
    //licbza kombinacji = n! / (k!*(n-k)!) = n!/ (2! * (n-2)!) = n! / (2 * (n-2)!)
    int rozmiar;
    int a = 1, b = 1;
    //n!
    for (int i = 1; i <= n; i++)
        a *= i;
    //cout << "a = " << a << endl;

    //(n-2)!
    for (int i = 1; i <= (n - 2); i++)
        b *= i;
    //cout << "b = " << b << endl;
    rozmiar = a / (2 * b);
    //cout << "rozmiar = " << rozmiar << endl;

    int roznica[rozmiar];
    int odleglosc[rozmiar];
    for (int i = 0; i < rozmiar; i++)
    {
        roznica[i] = 0;
        odleglosc[i] = 0;
    }

    //1. dla kazdej roznicy zapamietuje wynik i odleglosc
    //2. dla obliczonych roznic, szukam takiej ktora ma najwieksza odleglosc

    //1. dla kazdej roznicy zapamietuje wynik i odleglosc
    //cout << "n = " << n << endl;
    int k = 0;
    for (int i = 0; i < n; ++i)
        for (int j = (i + 1); j < n; ++j)
        {
            //cout << "k = " << k << endl;
            roznica[k] = abs(liczba[i] - liczba[j]); //abs - wartosc bezwzgledna
            if (roznica[k] != 0) //odrzucamy zera
            {
                odleglosc[k] = j - i;
                /*
                cout << "liczba[" << i << "] = " << liczba[i];
                cout << " " << "liczba[" << j << "] = " << liczba[j];
                cout << "  " << "roznica[" << k << "] = " << roznica[k];
                cout << "  " << "odleglosc[" << k << "] = " << odleglosc[k];
                cout << endl;
                */
            }
            k++;
        }

    //2. dla obliczonych roznic, szukam takiej ktora ma najwieksza odleglosc
    int maxi = 0;
    maxi = odleglosc[0];
    for (int i = 0; i < (rozmiar - 1); ++i)
    {
        for (int j = (i + 1); j < rozmiar; ++j)
        {
            maxi = max(maxi, odleglosc[j]);
            //cout << "maxi[" << i << "][" << j << "] = " << maxi << endl;
        }
    }

    if (maxi != 0)
        cout << maxi;
    else
        cout << "BRAK";

}
