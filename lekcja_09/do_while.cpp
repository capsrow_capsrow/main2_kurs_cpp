#include <iostream>
using namespace std;

int main() {
    int suma = 0;
    int a;
    do {
        cin >> a;
        suma += a;
    } while (a != 0);
    cout << suma << endl;
}
