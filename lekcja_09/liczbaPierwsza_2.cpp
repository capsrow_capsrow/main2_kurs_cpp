#include <iostream>
#include <cmath>
using namespace std;

bool czy_pierwsza(long long n) {
    int granica = int(sqrt(double(n)));
    for (int i = 2; i <= granica; ++i)
        if (n % i == 0)
            return false;
    return true;
}

int main() {
    long long n;
    cin >> n;
    cout << czy_pierwsza(n) << endl;
}
