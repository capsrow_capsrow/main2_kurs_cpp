#include <iostream>
using namespace std;

bool czy_pierwsza(long long n) {
    for (long long i = 2; i < n; ++i)
        if (n % i == 0)
            return false;
    return true;
}

int main() {
    long long n;
    cin >> n;
    cout << czy_pierwsza(n) << endl;
}
