#include <iostream>
#include <iomanip>

using namespace std;

//oprocentowanie lokaty w skali roku
//https://main2.edu.pl/main2/courses/show/6/24/
int main(){
    double kwota, procent;
    cin >> kwota >> procent;
    cout << setprecision(2) << fixed;//2 cyfry po kropce, fixed oznacza �e po przecinku
    cout << kwota + kwota * procent / 100.00 << endl;
}
