#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

int main(){
    double a,b;
    cin >> a >> b;
    cout << setprecision(2) << fixed;
    cout << sqrt(a * a + b * b) << endl;
}
