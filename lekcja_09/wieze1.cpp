#include <iostream>
#include <iomanip>

using namespace std;

/*
4
..W.
W...
....
...W
NIE

4
..W.
W...
..W.
...W
TAK
*/

//https://main2.edu.pl/main2/courses/show/6/24/
int main(){
    int n;
    cin >> n;
    string szach[n];

    for (int i = 0; i < n; ++i)
        cin >> szach[i];

    bool atak = false;

    for (int i = 0; i < n; ++i) //szachowanie wie� w pionie
    {
        int ile = 0;
        for (int j = 0; j < n; ++j)
            if (szach[i][j] == 'W')
                ++ile;
        if (ile > 1)
            atak = true;
    }

    for (int i = 0; i < n; ++i) {
        int ile = 0;
        for (int j = 0; j < n; ++j)
            if (szach[j][i] == 'W')
                ++ile;
        if (ile > 1)
            atak = true;
    }

    if (atak)
        cout << "TAK" << endl;
    else
        cout << "NIE" << endl;
}
