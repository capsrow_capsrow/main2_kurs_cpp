#include <iostream>
using namespace std;

int suma (int t[], int n){ //w zmiennej n jest przekazywany rozmiar tablicy
    int wynik = 0;
    for (int i = 0; i < n; i++)
        wynik += t[i];

    return wynik;
}

int main(){
    int n;
    cin >> n;
    int t[n];

    for (int i; i < n; i++)
        cin >> t[i];

    cout << suma (t, n) << endl;
}
