#include <iostream>
#include <string>
using namespace std;

// zadanie: https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/duz/

//liczba znakow w stringu, usuwanie wiodacych zer
//jezeli rozne ---> wynik jednoznaczny, oprocz wiodacych zer
//jezlei licza znakow taka sama
//porownaj najstarsze cyfry, potem mlodsze, potem najmlodsze

//usuwanie wiodacych zer z lancucha znakow
string usun_wiodace_zera(string we)
{
    string wy;
    int i, indeks;
    int len = we.size();

    for (i = len; i > 0; i--)
        if ( we [i] != '0')
        {
            indeks = i;
            i = 0;
        }

    for (i = indeks; i > 0 ; i--)
        wy += we[i];

    return wy;
}

//czy_rowne, obsluguje czy_rozne
bool czy_rowne(string x, string y){
    bool wyn;

    if ((x.size() > y.size()) || (y.size() > x.size()))
        wyn = false; //liczby x oraz y sa rozne
    else
    {
        for (int i = x.size(); i >= 0; i--)
            if (x[i] == y[i])
                wyn = true;
            else
            {
                wyn = false;
                i = -1;
            }
    }

    return wyn;
}

//czy_wieksze
bool czy_wieksze(string x, string y){
    bool wyn;

    if (x.size() > y.size())
        wyn = true; //liczba x jest wieksza od y
    else if (x.size() < y.size())
        wyn = false; //liczba x jest mniejsza od y
    else
    {
        for (int i = x.size(); i >= 0; i--)
            if (x[i] == y[i])
                wyn = false;
            else if (x[i] > y[i])
            {
                wyn = true;
                i = -1;
            }
            else
            {
                wyn = false;
                i = -1;
            }
    }

    return wyn;
}

//czy_mniejsze
bool czy_mniejsze(string x, string y){
    bool wyn;

    if (x.size() > y.size())
        wyn = false; //liczba x jest wieksza od y
    else if (x.size() < y.size())
        wyn = true; //liczba x jest mniejsza od y
    else
    {
        for (int i = x.size(); i >= 0; i--)
            if (x[i] == y[i])
                wyn = false;
            else if (x[i] > y[i])
            {
                wyn = false;
                i = -1;
            }
            else
            {
                wyn = true;
                i = -1;
            }
    }

    return wyn;
}


//czy_wieksze_rowne
bool czy_wieksze_rowne(string x, string y){
    bool wyn;

    if (x.size() > y.size())
        wyn = true; //liczba x jest wieksza od y
    else if (x.size() < y.size())
        wyn = false; //liczba x jest mniejsza od y
    else
    {
        for (int i = x.size(); i >= 0; i--)
            if (x[i] == y[i])
                wyn = true;
            else if (x[i] > y[i])
            {
                wyn = true;
                i = -1;
            }
            else
            {
                wyn = false;
                i = -1;
            }
    }

    return wyn;
}


//czy_mniejsze_rowne
bool czy_mniejsze_rowne(string x, string y){
    bool wyn;

    if (x.size() > y.size())
        wyn = false; //liczba x jest wieksza od y
    else if (x.size() < y.size())
        wyn = true; //liczba x jest mniejsza od y
    else
    {
        for (int i = x.size(); i >= 0; i--)
            if (x[i] == y[i])
                wyn = true;
            else if (x[i] > y[i])
            {
                wyn = false;
                i = -1;
            }
            else
            {
                wyn = true;
                i = -1;
            }
    }

    return wyn;
}

//duze liczby porownywanie: https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/duz/
//dzialania: rowne, rozne, wieksze, mniejsze, wieksze lub rowne, mniejsze lub rowne
int main(){
    string a, b, x, y;
    string operacja;
    bool wynik;

    cin >> a >> operacja >> b;

    x = usun_wiodace_zera(a);
    y = usun_wiodace_zera(b);

    if (operacja == "==")
        wynik = czy_rowne(x, y);
    else if (operacja == "!=")
        wynik = !(czy_rowne(x, y));
    else if (operacja == ">")
        wynik = czy_wieksze(x, y);
    else if (operacja == "<")
        wynik = czy_mniejsze(x, y);
    else if (operacja == ">=")
        wynik = czy_wieksze_rowne(x, y);
    else if (operacja == "<=")
        wynik = czy_mniejsze_rowne(x, y);

    if (wynik == true)
        cout << "TAK";
    else
        cout << "NIE";
}
