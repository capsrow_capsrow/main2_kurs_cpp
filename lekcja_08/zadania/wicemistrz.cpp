#include <iostream>
using namespace std;

const int MAX_N = 1000;

int t[MAX_N];
int n;

void sortowanie_przez_wybor(){
    for (int i = n - 1; i > 0; i--) {
        // wiemy, �e elementy a[i+1], a[i+2], ..., a[n-1] s� najwi�ksze w tablicy i s�
        // ju� uporz�dkowane: a[i+1] <= a[i+1] <= ... <= a[n-1] oraz �e dla ka�dego k = 0, 1, ..., i,
        // a[k] <= a[i+1]

        // wyznaczamy element najwi�kszy w podtablicy a[0..i]
        int m, maks;
        m = 0;
        maks = t[0];
        for (int j = 1; j <= i; j++)
            // maks == a[m] jest najwi�kszym elementem spo�r�d a[0], a[1], ..., a[j-1]
            if (t[j] > maks) {
                m = j;
                maks = t[m];
            }

        //zamieniamy element a[i] z elementem najwi�kszym
        t[m] = t[i];
        t[i] = maks;
    }
}

//wicemistrz: https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/wic/
int main(){
    cin >> n;

    for (int i = 0; i < n; i++)
        cin >> t[i];

    sortowanie_przez_wybor();

    cout << t[n-2] << endl;
}
