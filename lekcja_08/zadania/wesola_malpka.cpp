#include <iostream>
#include <string>
using namespace std;

//zadanie: https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/wes/


int main(){
    int z;//liczba zestawow danych a i b
    cin >> z;

    int a[z], b[z];//a - liczba klatek, b - dlugosc skoku

    for (int i = 0; i < z; i++)
        cin >> a[i] >> b[i];

    int suma, j, indeks;
    int liczbaKlatek, liczbaSkokow;
    for (int i = 0; i < z; i++)
    {
        suma = 0;
        liczbaKlatek = a[i];
        liczbaSkokow = 0;
        int x[liczbaKlatek];
        for (j = 1; j <= liczbaKlatek; j++) //wyzerowanie tablicy obliczeniowej
            x[j] = 0;

        for (j = 1; j <= liczbaKlatek + 1; j++)
        {
            suma += b[i];

            if (suma%liczbaKlatek == 0)
                indeks = liczbaKlatek;
            else
                indeks = suma%liczbaKlatek;

            //cout << "x[" << indeks << "]=" << x[indeks] << endl;
            if (x[indeks] == 0)
            {
                x[indeks] = 1;
                liczbaSkokow++;
            }
            else
            {
              cout << liczbaSkokow << endl;
              break;
            }

            if (suma > liczbaKlatek)
                j = indeks;
        }

    }

}
