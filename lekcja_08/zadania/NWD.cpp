#include <iostream>
using namespace std;

//algorytm Euklidesa: https://main2.edu.pl/main2/courses/show/6/21/
//funkcja szuka najwiekszego wspolnego dzielnika dwoch liczb calkowitych
//warunki: a jest nieujemne, b musi byc b > 0 bo w algorytnie dzielimy prze b
int NWD(int a, int b){
    int r = a % b;
    while (r != 0){
        a = b;
        b = r;
        r = a %b;
    }
    return b;
}

// NWD z liczb: https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/nwd/
int main(){
    int n;
    cin >> n;
    int t[n];

    for (int i = 0; i < n; i++)
        cin >> t[i];

    //rozwiazanie: https://mattomatti.com/pl/a0022
    int nwdv = t[0];
    for (int i = 1; i < n; i++)
        nwdv = NWD(nwdv, t[i]);

    cout << nwdv << endl;
}
