#include <iostream>
using namespace std;

int min(int a, int b){
    if (a < b)
        return a;
    else
        return b;
}

int main(){
    int x, y, z;
    cin >> x >> y >> z;
    cout << min(x + y, y *z) << endl;

    cout << min(3, 5) << endl;
}
