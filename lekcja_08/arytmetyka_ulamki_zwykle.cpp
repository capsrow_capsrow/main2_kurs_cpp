#include <iostream>
using namespace std;

//algorytm Euklidesa: https://main2.edu.pl/main2/courses/show/6/21/
//funkcja szuka najwiekszego wspolnego dzielnika dwoch liczb calkowitych
//warunki: a jest nieujemne, b musi byc b > 0 bo w algorytnie dzielimy prze b
int NWD(int a, int b){
    int r = a % b;
    while (r != 0){
        a = b;
        b = r;
        r = a %b;
    }
    return b;
}

//arytmetyka ulamkow zwyklych: suma, roznica, mnozenie, dzielenie: https://main2.edu.pl/main2/courses/show/6/21/
struct ulamek {
    int licznik;
    int mianownik;
};

//skracanie ulamka ulamek
ulamek skracanie (ulamek u){
    ulamek w;
    int x;

    w.licznik = u.licznik;
    w.mianownik = u.mianownik;

    if (w.licznik > 0)
        x = NWD(w.licznik, w.mianownik);
    else
        x = NWD(-w.licznik, w.mianownik);
    w.licznik /= x;
    w.mianownik /= x;

    return w;
}

//suma i roznoca, bo roznica to dodaj ze znakiemminus
ulamek suma (ulamek u1, ulamek u2){
    ulamek w;
    w.licznik = u1.licznik * u2.mianownik + u1.mianownik * u2.licznik;
    w.mianownik = u1.mianownik * u2.mianownik;
    if (w.licznik == 0)
        w.mianownik = 1;
    else
        w = skracanie(w);

    return w;
}

//iloczyn
ulamek iloczyn(ulamek u1, ulamek u2){
    ulamek w;
    w.licznik = u1.licznik * u2.licznik;
    w.mianownik = u1.mianownik * u2.mianownik;
    if (w.licznik == 0)
        w.mianownik = 1;
    else
        w = skracanie(w);

    return w;
}


//iloraz
ulamek iloraz(ulamek u1, ulamek u2){
    ulamek w;
    w.licznik = u1.licznik * u2.mianownik;
    w.mianownik = u1.mianownik * u2.licznik;
    if (w.licznik == 0)
        w.mianownik = 1;
    else
        w = skracanie(w);

    return w;
}

int main(){
    ulamek a, b;
    int x = 0, y = 0;
    ulamek wynik;

    cout << "podaj ulamek a: " << endl;
    cin >> x >> y;
    cout << endl;
    a.licznik = x;
    a.mianownik = y;
    x = y = 0;
    cout << "podaj ulamek b: " << endl;
    cin >> x >> y;
    cout << endl;
    b.licznik = x;
    b.mianownik = y;
    x = y = 0;

    wynik = suma(a, b);
    x = wynik.licznik;
    y = wynik.mianownik;
    cout << "suma = " << x << " " << y << endl;
    x = y = 0;
    b.mianownik = (-1) * b.mianownik;
    wynik = suma(a, b);
    x = wynik.licznik;
    y = wynik.mianownik;
    cout << "roznica = " << x << " " << y << endl;
    b.mianownik = (-1) * b.mianownik;
    x = y = 0;
    wynik = iloczyn (a, b);
    x = wynik.licznik;
    y = wynik.mianownik;
    cout << "iloczyn = " << x << " " << y << endl;
    x = y = 0;
    wynik = iloraz (a, b);
    x = wynik.licznik;
    y = wynik.mianownik;
    cout << "iloraz = " << x << " " << y << endl;
}
