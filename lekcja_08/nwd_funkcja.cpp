#include <iostream>
using namespace std;

//algorytm Euklidesa: https://main2.edu.pl/main2/courses/show/6/21/
//funkcja szyka najwiekszego wspolnego dzielnika dwoch liczb calkowitych
//warunki: a jest nieujemne, b musi byc b > 0 bo w algorytnie dzielimy prze b
int NWD(int a, int b){
    int r = a % b;
    while (r != 0){
        a = b;
        b = r;
        r = a %b;
    }
    return b;
}

int main(){
    int a, b;
    cin >> a >> b;
    cout << NWD(a, b) << endl;
}
