#include <iostream>
using namespace std;

int liczba_cyfr_1 (int n){ //bez zmiany n

    if (n == 0)
        return 1;

    int wynik = 0;
    while (n > 0){
        wynik++;
        n /= 10;
    }
    return wynik;
}

int liczba_cyfr_2 (int &n){ //przekazanie przez referencje, zatem modyfikowane jest zmienna globalna n

    if (n == 0)
        return 1;

    int wynik = 0;
    while (n > 0){
        wynik++;
        n /= 10;
    }
    return wynik;
}

int main(){
    int n;
    cin >> n;
    cout << liczba_cyfr_1(n) << endl;
    cout << n << endl;

    cout << liczba_cyfr_2(n) << endl;
    cout << n << endl;
}
