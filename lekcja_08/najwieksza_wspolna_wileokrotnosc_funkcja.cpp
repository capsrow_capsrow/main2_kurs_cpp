#include <iostream>
using namespace std;

//algorytm Euklidesa: https://main2.edu.pl/main2/courses/show/6/21/
//funkcja szuka najwiekszego wspolnego dzielnika dwoch liczb calkowitych
//warunki: a jest nieujemne, b musi byc b > 0 bo w algorytnie dzielimy prze b
int NWD(int a, int b){
    int r = a % b;
    while (r != 0){
        a = b;
        b = r;
        r = a %b;
    }
    return b;
}

//algorytm najwiekszej wspolnej wielokrotnosci
//NWW: : https://main2.edu.pl/main2/courses/show/6/21/
//wynikiem jest liczba wzglednie pierwsza
int NWW(int a, int b){
    return ((a + b) / NWD(a, b));
}

int main(){
    int a, b;
    cin >> a >> b;
    cout << NWW(a, b) << endl;
}
