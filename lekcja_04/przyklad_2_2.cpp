#include <iostream>
using namespace std;

int main() {
    int t;
    cin >> t;

    int g, m, s;

    g = t / 3600;
    t %= 3600;
    m = t /60;
    s = t % 60;

    cout << g << "g" << m << "m" << s << "s" << endl;
}
