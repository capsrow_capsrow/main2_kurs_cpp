#include <iostream>
using namespace std;

/* wielkanoc Gauss: https://pl.wikipedia.org/wiki/Wielkanoc#Dla_kalendarza_gregoria.C5.84skiego */
/* lata: 1800 < r < 2200 */
int main() {
    int A, B;
    int rok;

    cin >> rok;

    if (rok >= 1800 && rok <= 1899)
    {
        A = 23;
        B = 4;
    }
    else if (rok >= 1900 && rok <= 2099)
    {
        A = 24;
        B = 5;
    }
    else if (rok >= 2100 && rok <= 2199)
    {
        A = 24;
        B = 6;
    }

    int a, b, c, d, e;

    a = rok % 19;
    b = rok % 4;
    c = rok % 7;
    d = ((a * 19) + A) % 30;
    e = (2 * b + 4 * c + 6 * d + B) % 7;

    int wielkanoc_dzien, wielkanoc_miesiac;

    if (d == 29 && e == 6)
    {
        wielkanoc_dzien = 19;
        wielkanoc_miesiac = 4;
    }
    else if (d == 28 && e == 6 && a > 10 )
    {
        wielkanoc_dzien = 25;
        wielkanoc_miesiac = 4;
    }
    else if ((d + e) < 10)
    {
        wielkanoc_dzien = d + e + 22;
        wielkanoc_miesiac = 3;
    }
    else if ((d + e) >= 10)
    {
        wielkanoc_dzien = d + e - 9;
        wielkanoc_miesiac = 4;
    }

    cout << wielkanoc_dzien << " " << wielkanoc_miesiac <<endl;
}
