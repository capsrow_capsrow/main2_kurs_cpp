#include <iostream>
using namespace std;

int main() {
    int numertel, wiek;

    cin >> numertel >> wiek;

    int wynik = numertel % 10;
    wynik *= 2;
    wynik += 5;
    wynik *= 50;
    wynik += 1764;
    wynik -= wiek;

    cout << wynik << endl;
}


/*
rok lancuszka = 2014

rok urodzenia = 1998
A   B       C       D       E
nr  nr*2    nr*2+5  C*50    D+1764      E - rok urodzenia
0   0       5       250     2014        016
1   2       7       350     2114        116
2   4       9       450     2214        216
3   6       11      550     2314        316
4   8       13      650     2414        416
5   10      15      750     2514        516
6   12      17      850     2614        616
7   14      19      950     2714        716
8   16      21     1050     2814        816
9   18      23     1150     2914        916

*/

/*
łańcuszk na portalu społecznościowym
czemu służy:
materiał do nauki AI portalu społecznościowego
*/
