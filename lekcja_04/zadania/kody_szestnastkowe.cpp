#include <iostream>
using namespace std;

int main() {
    char hex_1, hex_2;
    int liczba_1, liczba_2;

    cin >> hex_1 >> hex_2;
//    cout << hex_1 << hex_2 << endl;

    //zamiana znaku na liczb�
    if (hex_1 >= 48 && hex_1 <= 57) //liczby 0-9
        liczba_1 = (int) hex_1 - 48;
    else if (hex_1 >= 65 && hex_1 <= 90) //litery A-F
        liczba_1 = (int) hex_1 - 55;
    else if (hex_1 >= 97 && hex_1 <= 102) //litery a-f
        liczba_1 = hex_1 - 87;

    if (hex_2 >= 48 && hex_2 <= 57) //liczby 0-9
        liczba_2 = (int) hex_2 - 48;
    else if (hex_2 >= 65 && hex_2 <= 90) //litery A-F
        liczba_2 = (int) hex_2 - 55;
    else if (hex_2 >= 97 && hex_2 <= 102) //litery a-f
        liczba_2 = hex_2 - 87;

    int wynik_liczba;
    wynik_liczba = 16 * liczba_1 + liczba_2;

    cout << wynik_liczba << endl; //kod ASCII
    cout << (char) wynik_liczba << endl; //znak

}
