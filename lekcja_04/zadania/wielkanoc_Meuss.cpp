#include <iostream>
using namespace std;

/* wielkanoc Gauss: https://pl.wikipedia.org/wiki/Wielkanoc#Dla_kalendarza_gregoria.C5.84skiego */
/* lata: 1800 < r < 2200 */
int main() {
    int rok;

    cin >> rok;

    int a, b, c, d, e, f, g, h, i, k, l, m, n, p;

    a = rok % 19;
    //cout << "a = " << a << endl;
    b = rok / 100;
    //cout << "b = " << b << endl;
    c = rok % 100;
    //cout << "c = " << c << endl;
    d = b / 4;
    //cout << "d = " << d << endl;
    e = b % 4;
    //cout << "e = " << e << endl;
    f = (b + 8) / 25;
    //cout << "f = " << f << endl;
    g = (b - f + 1) / 3;
    //cout << "g = " << g << endl;
    h = (19 * a + b - d - g + 15) % 30;
    //cout << "h = " << h << endl;
    //cout << "h przed podzialem przez 30 = " << (19 * a + b - d - g + 15) << endl;
    i = c / 4;
    //cout << "i = " << i << endl;
    k = c % 4;
    //cout << "k = " << k << endl;
    l = (32 + 2 * e + 2 * i - h - k) % 7;
    //cout << "l = " << l << endl;
    m = (a + 11 * h + 22 * l) / 451;
    //cout << "m = " << m << endl;
    p = (h + l - 7 * m + 114) % 31;
    //cout << "p = " << p << endl;

    int wielkanoc_dzien, wielkanoc_miesiac;

    wielkanoc_dzien = p + 1;
    wielkanoc_miesiac = (h + l - 7 * m + 114) / 31;

    cout << wielkanoc_dzien << " " << wielkanoc_miesiac <<endl;
}
