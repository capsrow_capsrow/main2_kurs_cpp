#include <iostream>
using namespace std;

/* ile liczb z przedzia�u obustronnie domkni�tego od a do b jest podzielnych przez k. */
int main() {

    int a, b, k;

    cin >> a >> b >> k;

    int dodaj_do_a, najmniejsza_podzielna;
    if (a > k)
        dodaj_do_a = a % k;
    else
        dodaj_do_a = k % a;
    najmniejsza_podzielna = a + dodaj_do_a;

    int odejmij_od_b, najwieksza_podzielna;
    if (b > k)
        odejmij_od_b = b % k;
    else
        odejmij_od_b = k % b;
    najwieksza_podzielna = b - odejmij_od_b;

    int wynik;
    wynik = (najwieksza_podzielna - najmniejsza_podzielna) / k;
    wynik += 1;

    cout << wynik << endl;
}
