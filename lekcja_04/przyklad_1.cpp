/*
Alina m�wi do Bolka: rzu� trzema kostkami, zapami�taj wyniki i nie pokazuj mi.
Pierwszy pomn� przez 2, dodaj 5, pomn� przez 5, dodaj wynik z drugiej kostki, pomn� przez 10,
na koniec dodaj wynik z trzeciej kostki i powiedz, ile Ci wysz�o.
*/

#include <iostream>
using namespace std;

int main() {
    int kostka_1, kostka_2, kostka_3;

    cin >> kostka_1 >> kostka_2 >> kostka_3;

    int w = kostka_1; //zmienna pomocnicza
    w = w * 2;
    w = w + 5;
    w = w * 5;
    w = w + kostka_2;
    w = w * 10;
    w = w + kostka_3;

    cout << w << endl;

}
