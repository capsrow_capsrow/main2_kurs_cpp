#include <iostream>
using namespace std;

//zadanie: https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/pom/
int main(){
    int n = 0;
    cin >> n;

    int x = 0;
    while (n > 0)
    {
        x = n % 10;
        n = n / 10;
        if (x != 0)
            cout << x;
    }
}
