#include <iostream>
using namespace std;

//zadanie: https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/pom/
int main(){
    int p = 0, q = 0;
    int wynik = 0;

    while (p >= 0)
    {
        cin >> p;
        if (p > 0 && q != p)
            wynik++;
        q = p;
    }

    cout << wynik << endl;
}
