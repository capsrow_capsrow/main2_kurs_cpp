#include <iostream>
using namespace std;

//zadanie: https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/pot/
int main(){
    int n;
    cin >> n;

    int wynik = 0;

    while ((wynik * 2) < n)
    {
        wynik *= 2;
        if (wynik == 0)
            wynik ++;

        cout << wynik << endl;
    }
}
