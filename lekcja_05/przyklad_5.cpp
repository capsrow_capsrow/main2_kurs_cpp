#include <iostream>
using namespace std;

int main() {
    int n; //liczba naturalna
    cin >> n;
    int liczba_cyfr = 0;

    if (n == 0)
        liczba_cyfr = 1;
    else
        while (n > 0){
            n /= 10;//obciecie ostatniej cyfry zmiennej n
            liczba_cyfr++;
        }

    cout << liczba_cyfr << endl;
}
