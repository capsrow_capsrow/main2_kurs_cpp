#include <iostream>
using namespace std;
/* problem Ulama:
   dlaczego zawsze sie zakonczy
   https://pl.wikipedia.org/wiki/Problem_Collatza
*/

int main(){
    int i;
    cin >> i;
    while (i != 1)
        if (i % 2 == 0) //i jest parzyste
            i = i / 2;
        else
            i = 3 * i + 1;

    cout << "KONIEC" << endl;
}
