#include <iostream>
using namespace std;

//na przemian: https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/nap/
int main() {
    int n = 0;
    cin >> n;

    int t[n];

    for (int i = 0; i < n; i++)
        cin >> t[i];

    for (int i = 0; i < n; i++)
        if ((i % 2) == 0)
            cout << t[i] << " ";
    cout << endl;

    for (int i = 0; i < n; i++)
        if ((i % 2) != 0)
            cout << t[i] << " ";
    cout << endl;

}


