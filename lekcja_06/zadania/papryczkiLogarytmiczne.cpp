#include <iostream>
#include <cmath>
using namespace std;

//papryczki logarytmiczne: https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/pap/
int main() {
    int k = 0;
    cin >> k;

    int liczbapapryczek[k], wynik[k];
    int i, j;
    int maxi;

    for (i = 0; i <= k; i++)
        cin >> liczbapapryczek[i];

    /*
    for (i = 0; i <= k; i++)
        cout << "liczbapapryczek[" << i << "]" << liczbapapryczek[i] << endl;
    */

    int zamowienie = 0;
    if (liczbapapryczek[0] == 0)
        zamowienie = 1;
    else
    {
        for (i = 0; i <= k; i++)
        {
            wynik[i] = liczbapapryczek[i]*pow(2, i);
            //cout << "wynik [" << i << "] = " << wynik[i] << endl;
        }

        for (i = 0; i <= k; i++)
            if (wynik[i] == 0)
            {
                maxi = pow(2, i);
                //cout << "maxi [" << i << "] = " << maxi << endl;
                for (j=0; j <= i; j++)
                    if (liczbapapryczek[j]*pow(2, j) >= maxi) //uda si� uzyska� wynik, mimo �e waga przy 2^i jest zerowa
                    {
                        zamowienie = maxi + 1;
                        j = k +1; //przerwanie petli
                        i = k + 1;//przerwanie petli
                    }
                    else
                    {
                        //cout << "j wejscie = " << j << endl;
                        zamowienie = maxi;
                        j = k + 1;//przerwanie petli
                        i = k + 1;//przerwanie petli
                        //cout << "zamowienie = " << zamowienie << endl;
                        //cout << "j wyjscie = " << j << endl;
                    }
            }
            else
            {
                zamowienie += wynik[i];
                if (i == k)
                    zamowienie++;
            }
    }

    cout << endl << zamowienie;

/*
0 1 2 3
0 0 0 2

dla 0 * 2^0 -> nie osiagniemy wagi rownej 2^0 = 1, nie osiagniemy zadnej wagi nieparzystej
dla N * 2^0 -> nie osiagniemy wagi wiekszej od N*2^0
dla 0 * 2^1 -> nie osiagniemy wagi rownej 2^1 = 2
dla N * 2^1 -> nie osiagniemy wagi wiekszej od N*2^0
dla 0 * 2^k -> nie osiagniemy wagi rownej 2^k
dla N * 2^k -> nie osiagniemy wagi wiekszej 2^k

je�eli gdziekolwiek N = 0, to nie osiagniemy wagi r�wnej 2^k
ale sprawdzamy czy suma mniejszych poteg jest wieksza od 2^k

*/


}



