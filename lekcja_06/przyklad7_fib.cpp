#include <iostream>
using namespace std;

int main() {
    long long n = 0;
    cin >> n;


    unsigned long long fib[n];
    fib[0] = 0;
    fib[1] = 1;


    for (int i = 2; i < n; i++)
    {
        fib[i] = fib[i-1] + fib[i-2];
        cout << "fib[" << i << "] = " << fib[i] << endl;
    }


    cout << fib[n-1] << endl;
}
