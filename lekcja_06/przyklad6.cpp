#include <iostream>
using namespace std;

int main() {
    int n;
    cin >> n;
    int liczba_cyfr = 0;
    if (n == 0)
        liczba_cyfr = 1;
    else
        for (; n > 0; n /= 10)
            liczba_cyfr++;
    cout << liczba_cyfr << endl;
}
