#include <iostream>
using namespace std;

int main() {
    int n;
    cin >> n;
    int t[n];

    for (int i = 0; i < n; i++)
        cin >> t[i];

    int suma = 0;
    for (int i = 0; i < n; i++)
        suma += t[i];
    cout << suma << endl;

    int min = t[0];
    for (int i = 1; i < n; i++)
        if (t[i] < min)
            min = t[i];
    cout << min << endl;
}
