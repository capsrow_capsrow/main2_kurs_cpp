//wyszukiwanie wzorca w tekscie
#include <iostream>
#include <string>
using namespace std;

int main() {
    string wzorzec, tekst;
    cin >> wzorzec >> tekst;

    int m = wzorzec.size(), n = tekst.size();
    for (int i = 0; i <= n - m; i++)
    {
        bool pasuje = true;
        for (int j = 0; j < m; j++)
            if (wzorzec[j] != tekst[i + j])
                pasuje = false;
        if (pasuje)
            cout << i << endl;
    }
}
