//sprawdzenie czy ciag jest rosnacy
#include <iostream>
using namespace std;

int main() {
    int n;
    cin >> n;
    int t[n];
    for (int i = 0; i < n; i++)
        cin >> t[i];

    bool czyciagjestroznacy = false;

    for (int i = 1; i < n; i++)
        if (t[i-1] < t[i])
            czyciagjestroznacy = true;
        else
            czyciagjestroznacy = false;

    if (czyciagjestroznacy)
        cout << "TAK" << endl;
    else
        cout << "NIE" << endl;
}
