//tabliczka mnozenia
#include <iostream>
using namespace std;

int main() {
    // 1 * 1, 1 * 2, ..., 1 * 9
    // 2 * 1, 2 * 2, ..., 2 * 9

    for (int i = 1; i <= 9; i++)
    {
        for (int j = 1; j <= 9; j++)
        {
            if (i * j < 10)
                cout << " ";
            cout << i * j << " ";
        }
        cout << endl;
    }
}
