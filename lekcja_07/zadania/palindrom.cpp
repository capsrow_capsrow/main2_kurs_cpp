#include <iostream>
#include <string>
using namespace std;

//palindrom: https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/pal/
//
int main() {
    string tekst, odwrocony;
    cin >> tekst;
    int s = tekst.size(), i;

    for (i = s - 1; i >= 0; i--)
        odwrocony += tekst[i];

    if (tekst == odwrocony)
        cout << "TAK" << endl;
    else
        cout << "NIE" << endl;
}
