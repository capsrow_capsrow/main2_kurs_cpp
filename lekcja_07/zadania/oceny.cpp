#include <iostream>
using namespace std;

//oceny: https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/oce/
//
int main() {
    int n;
    cin >> n;
    int oceny[n], liczbaocen[6], i, ocena;
    for (i = 0; i < n; i++)
        cin >> oceny[i];

    //zerowanie liczby ocen
    for (i = 0; i <= 6; i++)
        liczbaocen[i] = 0;

    for (i = 0; i < n; i++)
    {
        ocena = oceny[i];
        liczbaocen[ocena]++;
    }

    for (i = 1; i <= 6; i++)
        cout << liczbaocen[i] << " ";
}
