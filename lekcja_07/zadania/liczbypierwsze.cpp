#include <iostream>
#include <cmath>
using namespace std;

//liczby pierwsze: https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/lic/
//algortym: http://www.algorytm.edu.pl/algorytmy-maturalne/badanie-czy-liczba-pierwsza.html
int main() {
    int n = 0;
    int i = 2;
    cin >> n;

    int pierwiastek = sqrt(n);
    bool czypierwsza = true;

    if (n < 2 )
        czypierwsza = true;
    else
    {
        for (int i = 2; i <= pierwiastek; i++)
            if (n%i == 0)
                czypierwsza = false;
            else
            {
                czypierwsza = true;
                i = pierwiastek+2; //ustawienie warunku na wyjscie z petli
            }
    }

    if (czypierwsza)
        cout << "pierwsza" << endl;
    else
        cout << "zlozona" << endl;
}



