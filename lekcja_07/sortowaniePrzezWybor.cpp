    //sortowanie przez wybor
#include <iostream>
using namespace std;

int main() {
    int n;
    cin >> n;
    int a[n];
    int i, j;
    for (i = 0; i < n; i++)
        cin >> a[i];

    for (i = n - 1; i > 0; i--) {
        // wiemy, �e elementy a[i+1], a[i+2], ..., a[n-1] s� najwi�ksze w tablicy i s�
        // ju� uporz�dkowane: a[i+1] <= a[i+1] <= ... <= a[n-1] oraz �e dla ka�dego k = 0, 1, ..., i,
        // a[k] <= a[i+1]

        // wyznaczamy element najwi�kszy w podtablicy a[0..i]
        int m, maks;
        m = 0;
        maks = a[0];
        for (j = 1; j <= i; j++)
            // maks == a[m] jest najwi�kszym elementem spo�r�d a[0], a[1], ..., a[j-1]
            if (a[j] > maks) {
                m = j;
                maks = a[m];
            }

        //zamieniamy element a[i] z elementem najwi�kszym
        a[m] = a[i];
        a[i] = maks;
    }

    for (i = 0; i < n; i++)
        cout << a[i] << endl;

}
