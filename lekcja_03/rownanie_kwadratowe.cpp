#include <iostream>
#include <cmath> //funkcje potega -> pow, pierwiastek -> sqrt
using namespace std;

/*
a*x^2 + b*x + c = 0 /podziel przez a
x^2 + b/a * x + c/a = 0

wlasnosc: (p+q)^2 = p^2 + 2*p*q + q^2
2pq = b/a*x i q = b/2*a

x^2 + b/a * x + b^2/4*a^2 - b^2/4*a^2 + c/a = 0
(x + b/2a)^2 - b^2/4*a^2 + c/a = 0
(x + b/2a)^2 = b^2/4*a^2 - c/a ==> sprowadzamy prawa strone do wspolnego mianownika
(x + b/2a)^2 = (b^2 - 4 * a * c) /4*a^2
delta = b^2 - 4 * a * c
(x + b/2a)^2 = delta / 4*a^2

delta <0    -> r. kwadratowe nie ma rozwiazan w dziedzinie liczb rzeczywistych
delta = 0   -> r. kwadratowe ma rozwiazanie (x + b/2a)^2 = 0, x = -b/2a
delta > 0   -> r. kwadratowe ma dwa rozwiazania:
                    x_1 + b/2a = sqrt(delta)/2a
                    x_1 = (-b + sqrt(delta))/2a
                    x_2 + b/2a = -sqrt(delta)/2a
                    x_2 = (-b - sqrt(delta))/2a


*/

int main () {
    long double a, b, c; //wapolczynniki rownania kwadratowego, a != 0
    cin >> a >> b >> c;

    if (a == 0)
        cout << "a nie moze byc rowne 0" << endl;
    else
    {
        long double delta;
        delta = pow(b, 2) - (4 * a * c);

        long double x_1, x_2;

        if (delta < 0)
            cout << "r. kwadratowe nie ma rozwiazan w dziedzinie liczb rzeczywistych" << endl;
        else if (delta == 0)
        {
            x_1 = ((-1) * b) / (2 * a);
            cout << "r. kwadratowe ma jedno rozwiazanie = " << x_1 << endl;
        }
        else if (delta > 0)
        {
            x_1 = ((-1)*b + sqrt(delta)) / (2 * a);
            x_2 = ((-1)*b - sqrt(delta)) / (2 * a);
            cout << "r. kwadratowe ma dwa rozwiazania: x_1 = " << x_1 << " ; x_2 = " << x_2 << endl;
        }
    }
}
