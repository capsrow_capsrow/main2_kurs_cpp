#include <iostream>
using namespace std;

int main () {
    int a, b, k;
    cin >> a >> b >> k;

    /*
    a = 15
    b = 18
    k = 4

    bok a:
    liczba krzesel: a / k

    bok b:
    liczba krzesel: (b - 2k) / k
    */

    int wynik;
    wynik = (a / k) + ((b - 2*k) / k);
    wynik *= 2;

    if (wynik >= 0)
        cout << wynik << endl;
    else
        cout << 0 << endl;
}
