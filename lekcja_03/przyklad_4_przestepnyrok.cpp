#include <iostream>
using namespace std;

int main () {
    /*
    2012 - rok jest przestepny jezeli dzieli sie przez 4
    2014 - rok nie jest przestepny jezeli nie dzieli sie przez 4
    2100, 2200 - rok nie jest przestepny jezeli dzieli sie przez 4 i dzieli sie przez 100
    2100, 2200 - rok jest przestepny jezeli dzieli sie przez 4 i dzieli sie przez 100 i dzieli sie przez 400
    */
    int rok;
    cin >> rok;
    if ((rok % 4 == 0 && !(rok % 100 == 0)) || rok % 400 == 0)
        cout << 366 << endl;
    else
        cout << 365 << endl;
}

