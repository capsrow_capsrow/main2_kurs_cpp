// instalacja allegro w CodeBlocks
// http://cpp0x.pl/forum/temat/?id=19634

#include <allegro5/allegro.h> // plik nag��wkowy potrzebny do u�ywania biblioteki
using namespace std;

// szeroko�� i wysoko�� okna
const int SZER = 640;
const int WYS = 480;

int main_1() {
    al_init(); // inicjacja biblioteki - niezb�dna na pocz�tek
    // poni�sza funkcja tworzy okno o podanych rozmiarach i zwraca wska�nik na to okno;
    // przypisali�my ten wska�nik do zmiennej "okno"
    ALLEGRO_DISPLAY *okno = al_create_display(SZER, WYS);
    al_install_keyboard(); // inicjacja klawiatury - je�li chcemy jej u�ywa�
    ALLEGRO_KEYBOARD_STATE klawiatura; // zmienna reprezentuj�ca stan klawiatury
    do {
        al_clear_to_color(al_map_rgb_f(1.0, 1.0, 0.0)); // ustawienie koloru okna - patrz komentarz powy�ej
        al_flip_display(); // wy�wietlenie wszystkiego, co narysowali�my do tej pory, na ekran
        // pobranie bie��cego stanu klawiatury;
        // funkcja przyjmuje wska�nik na stan klawiatury, wi�c piszemy "&"
        al_get_keyboard_state(&klawiatura);
    } while (!al_key_down(&klawiatura, ALLEGRO_KEY_ESCAPE)); // czy wci�ni�to klawisz Esc
    al_destroy_display(okno); // funkcja zamykaj�ca okno
    return 0;
}
