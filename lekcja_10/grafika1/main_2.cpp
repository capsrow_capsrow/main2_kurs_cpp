#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h> // za��czamy modu� primitives
using namespace std;

const int SZER = 640;
const int WYS = 480;

int main_2() {
    al_init();
    al_init_primitives_addon(); // inicjujemy modu� primitives
    ALLEGRO_DISPLAY *okno = al_create_display(SZER, WYS);
    al_install_keyboard();
    ALLEGRO_KEYBOARD_STATE klawiatura;
    do {
        al_clear_to_color(al_map_rgb_f(1.0, 1.0, 0.6));
        int x = SZER / 2, y = WYS / 2, r = 30;
        al_draw_filled_circle(x, y, r, al_map_rgb_f(0.0, 0.0, 1.0)); // k�ko.
        al_flip_display();
        al_get_keyboard_state(&klawiatura);
    } while(!al_key_down(&klawiatura, ALLEGRO_KEY_ESCAPE));
    al_destroy_display(okno);
    return 0;
}
