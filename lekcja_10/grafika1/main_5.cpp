#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
using namespace std;

const int SZER = 640;
const int WYS = 480;

int main() {
    al_init();
    al_init_primitives_addon();
    ALLEGRO_DISPLAY *okno = al_create_display(SZER, WYS);
    al_install_keyboard();
    ALLEGRO_KEYBOARD_STATE klawiatura;
    int x = SZER / 2, y = WYS / 2, r = 30;
    do {
        al_clear_to_color(al_map_rgb_f(1.0, 1.0, 0.6)); // RGB
        al_draw_filled_circle(x, y, r, al_map_rgb_f(0.0, 0.0, 1.0));
        al_flip_display();
        al_get_keyboard_state(&klawiatura);
        if (al_key_down(&klawiatura, ALLEGRO_KEY_RIGHT) && x + r < SZER) ++x;
        if (al_key_down(&klawiatura, ALLEGRO_KEY_LEFT)  && x - r > 0)    --x;
        if (al_key_down(&klawiatura, ALLEGRO_KEY_DOWN)  && y + r < WYS)  ++y;
        if (al_key_down(&klawiatura, ALLEGRO_KEY_UP)    && y - r > 0)    --y;
        al_rest(0.001);
    } while(!al_key_down(&klawiatura, ALLEGRO_KEY_ESCAPE));
    al_destroy_display(okno);
    return 0;
}
