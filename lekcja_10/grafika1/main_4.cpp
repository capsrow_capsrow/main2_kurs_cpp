#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
using namespace std;

const int SZER = 640;
const int WYS = 480;

int main_4() {
    al_init();
    al_init_primitives_addon();
    ALLEGRO_DISPLAY *okno = al_create_display(SZER, WYS);
    al_install_keyboard();
    ALLEGRO_KEYBOARD_STATE klawiatura;
    int x = SZER / 2, y = WYS / 2, r = 30;
    int dx = 1, dy = 1;
    do {
        al_clear_to_color(al_map_rgb_f(1.0, 1.0, 0.6)); // RGB
        al_draw_filled_circle(x, y, r, al_map_rgb_f(0.0, 0.0, 1.0));
        x += dx; y += dy;
        if (x + r == SZER) dx = -1;
        if (y + r == WYS)  dy = -1;
        if (x - r == 0)    dx =  1;
        if (y - r == 0)    dy =  1;
        al_rest(0.001);
        al_flip_display();
        al_get_keyboard_state(&klawiatura);
    } while(!al_key_down(&klawiatura, ALLEGRO_KEY_ESCAPE));
    al_destroy_display(okno);
    return 0;
}
