#include <iostream>

//zadanie: https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/bit/
/*
CIAG BITONICZNY: ciag, kt�ry lub na pocz�tku jest nierosnacy,
                 a p�niej niemalejacy, lub na pocz�tku niemalejacy,
                 a p�niej nierosnacy.
                 C.b. sa uzywane w sieciach sortujacych
*/
using namespace std;

/*
1. bierzesz pierwszy element
2. bierzesz kolejny element dop�ki jest wi�kszy od poprzedniego
3. bierzesz kolejny element dop�ki jest mniejszy od poprzedniego
4. je�li dochodzisz do ostatniego elementu to jest bitoniczny, a jak nie to nie jest bitoniczny
*/

int main(){
    int n;
    cin >> n; // dlugosc ciagu

    int x[n]; //wyrazy ciagu
    for (int i = 0; i < n; ++i)
        cin >> x[i];

    bool czybitoniczny = false;
    bool czymaleje = false;
    for (int i = 0; i < n -1; ++i)
    {
        if ((x[i+1] > x[i]))
        {
         if (czymaleje == false)
            czybitoniczny = true;
         else
            czybitoniczny = false;
        }
        else if (x[i+1] < x[i])
        {
            czymaleje = true;
            czybitoniczny = true;
        }
        else
            czybitoniczny = false;
    }

    if (czybitoniczny == true)
        cout << "TAK";
    else
        cout << "NIE";
}
