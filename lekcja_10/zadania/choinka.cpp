#include <iostream>

//zadanie:https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/cho/
using namespace std;

int policzgwiazdki (int liczba)
{
    int wynik;
    wynik = liczba + 2;
    return wynik;
}

string rysujchoinke(int liczbagwiazdek, int liczbaspacji){
    string wynik;
    for (int i = 1; i <= liczbaspacji; ++i)
        wynik += " ";

    for (int i = 1; i <= liczbagwiazdek; ++i)
        wynik += "*";

    return wynik;
}

int main(){
    int n;
    cin >> n; // rozmiar choinki, czyli liczba wierszy choinki

    int galezie[n+1], spacje[n+1];//n-1 na galezie, n i n+1 na pieniek

    galezie[0] = 1;
    spacje[0]  = n - 1;
    for (int i = 1; i < n; ++i)
    {
        galezie[i] = policzgwiazdki(galezie[i-1]);
        spacje[i]  = n - i - 1;
    }
    galezie[n]   = 1;
    spacje[n]    = spacje[0];
    galezie[n+1] = 1;
    spacje[n+1]  = spacje[0];

    string choinka[n+2];
    int i;
    for (i = 0; i < n + 2; i++)
        choinka[i] = (string) rysujchoinke(galezie[i], spacje[i]);

    for (i = 0; i <= n + 2; ++i)
        cout << choinka[i] << endl;
}
