#include <iostream>
#include <algorithm>

//zadanie: https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/sad/
using namespace std;

int mintablica (int t[], int rozmiar)
{
    int minlocal = t[0];
    for (int i = 0; i < rozmiar; ++i)
        minlocal = min(minlocal, t[i]);

    return minlocal;
}

int maxtablica (int t[], int rozmiar)
{
    int maxlocal = t[0];
    for (int i = 0; i < rozmiar; ++i)
        maxlocal = max(maxlocal, t[i]);

    return maxlocal;
}

int main(){
    int n;
    cin >> n; // liczba jabloni

    int x[n], y[n]; //wspolrzedne jabloni
    for (int i = 0; i < n; ++i)
        cin >> x[i] >> y [i];

    //znajd� min X i Y, oraz max X i Y
    //nastepnie oblicz pole prostokata
    //znajduje minimum w tablicy x[i]
    int minX = mintablica(x, n);
    int minY = mintablica(y, n);
    int maxX = maxtablica(x, n);
    int maxY = maxtablica(y, n);

    int obwod = 2 * ((maxX - minX) + (maxY - minY));
    cout << obwod << endl;
}
