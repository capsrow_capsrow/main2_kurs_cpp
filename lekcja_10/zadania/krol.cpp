#include <iostream>

using namespace std;

//zadanie: https://main2.edu.pl/c/konkurs-wstepu-do-programowania/p/kro/
int main(){
    //szachownica 8x8
    char k; //symbol kolumny:  a b c d e f g h
    int w;  //symbol wiersza:  1 2 3 4 5 6 7 8

    cin >> k >> w;

    //  'a' <= k <= 'h'
    //   1  <= w <=  8
    //   x x x
    //   x   x
    //   x x x

    int wynik = 8;

    char lewo, prawo;
    int gora, dol;
    lewo    =  k - 1;
    prawo   =  k + 1;
    gora    =  w - 1;
    dol     =  w + 1;
    if ((lewo < 'a' && gora < 1) || (prawo > 'h' && gora < 1) || (lewo < 'a' && dol > 8) || (prawo > 'h' && dol > 8))
        wynik -= 5;
    else if ((lewo < 'a' && gora >= 1) || (lewo < 'a' && dol <= 8) || (prawo > 'h' && gora >= 1) || (prawo > 'h' && dol <= 8))
        wynik -= 3;

    cout << wynik;

}
