// Rozmiary typ�w danych
//--------------------------------

#include <iostream>

using namespace std;

int main()
{
    cout << "                   int : " << sizeof (int) << endl
         << "          unsigned int : " << sizeof (unsigned) << endl
         << "             short int : " << sizeof (short) << endl
         << "    unsigned short int : " << sizeof (unsigned short) << endl
         << "                  char : " << sizeof (char) << endl
         << "         unsigned char : " << sizeof (unsigned char) << endl
         << "         long long int : " << sizeof (long long) << endl
         << "unsigned long long int : " << sizeof (unsigned long long) << endl
         << "                 float : " << sizeof (float) << endl
         << "                double : " << sizeof (double) << endl
         << "           long double : " << sizeof (long double) << endl
         << "                  bool : " << sizeof (bool) << endl;

    return 0;
}
